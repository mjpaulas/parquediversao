-- TRIGGER 1: Se o cliente for menor de 12 anos ou maior de 75 anos, conferir se foi inserido um CPF de responsável. Caso contrário, não inserir a tupla na relação.

CREATE OR REPLACE FUNCTION confere_cpf_responsavel() 
RETURNS TRIGGER AS $$
BEGIN
	IF (AGE(NEW.data_nasc) < '12 years' OR AGE(NEW.data_nasc) > '75 years') THEN
		IF (NEW.cpf_responsavel = '') THEN
			RAISE EXCEPTION 'Esse cliente precisa de um responsável por ele!';
		END IF;
	END IF;
    RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER T_cpf_responsavel
AFTER INSERT ON cliente
FOR EACH ROW
EXECUTE PROCEDURE confere_cpf_responsavel();

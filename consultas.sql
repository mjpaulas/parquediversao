-- CONSULTA 1: Listar todos os funcionários que monitoram brinquedos que são Montanhas Russas. 
select nome_func from monitor natural join brinquedo natural join funcionario where nome_brinquedo LIKE 'Montanha Russa%';

-- CONSULTA 2: Listar o nome do vendedor que mais vendeu ingressos em determinado período. 
 
CREATE TEMPORARY TABLE ingressos_vendidos(
	cpf_func varchar(11),
	numero_ingressos bigint 
);

-- Primeiro pegar quais foram os ingressos vendidos no período de tempo e inserir na tabela temporária.
INSERT INTO ingressos_vendidos(SELECT ingressos.cpf_func, COUNT(*) FROM ingressos WHERE '[1990-11-10, 2040-11-20]'::daterange @> data_compra 
	GROUP BY ingressos.cpf_func);
	
-- Ver qual foi o funcionário que mais vendeu ingressos.
SELECT DISTINCT nome_func FROM funcionario natural join ingressos_vendidos WHERE numero_ingressos = (SELECT MAX(numero_ingressos) FROM ingressos_vendidos); 

-- CONSULTA 3: Quantos clientes maiores de 75 anos frequentaram o parque em determinado período de tempo?
CREATE TEMPORARY TABLE ingressos_usados(
	cpf_cliente varchar(11) 
);

-- Primeiro pegar quais foram os ingressos USADOS no período de tempo e inserir na tabela temporária.
INSERT INTO ingressos_usados(SELECT ingressos.cpf_cliente FROM ingressos WHERE '[1990-11-10, 2040-11-20]'::daterange @> data_uso);
SELECT COUNT(*) FROM cliente natural join ingressos_usados WHERE AGE(data_nasc) >= '75 years';
	
-- CONSULTA 4: Quais funcionários venderam ingressos para clientes dependentes em determinado período de tempo?
-- Tabela temporária para inserir o CPF de clientes dependentes que compraram o ingresso em determinado período de tempo.
CREATE TEMPORARY TABLE clientes_dependentes_ingresso(
	cpf_cliente varchar(11) 
);

INSERT INTO clientes_dependentes_ingresso (
	SELECT cpf_cliente from cliente natural join ingressos 
	WHERE AGE(data_nasc) < '12 years' OR AGE(data_nasc) >= '75 years' AND '[1990-11-10, 2040-11-20]'::daterange @> data_compra);
	
SELECT nome_func FROM funcionario natural join ingressos natural join clientes_dependentes_ingresso;
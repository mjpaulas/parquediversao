CREATE TABLE BRINQUEDO (
	codigo_brinquedo varchar(5) PRIMARY KEY, -- Tem como a gente ver um CHECK 5 caracteres?
	nome_brinquedo varchar(50) NOT NULL,
	hora_inicio time NOT NULL CHECK (hora_inicio >= '10:00'),
	hora_fim time NOT NULL CHECK (hora_fim <= '20:00'),
	idade_min smallint NOT NULL CHECK (idade_min >= 0),
	idade_max smallint DEFAULT 100 NOT NULL CHECK (idade_max <= 100), -- Colocar um DEFAULT	
	capacidade_max smallint NOT NULL CHECK (capacidade_max >= 0)
);

CREATE TABLE FUNCIONARIO (
	cpf_func varchar(11) PRIMARY KEY, 
	tipo_func varchar(20) NOT NULL CHECK (tipo_func IN ('Monitor', 'Vendedor')), -- Só existirão dois tipos de funcionário? 
	nome_func varchar(50) NOT NULL, 
	tel1 varchar(11),
	tel2 varchar(11)
);

CREATE TABLE MONITOR (
	cpf_func varchar(11) PRIMARY KEY, 
	codigo_brinquedo varchar(5) NOT NULL,
	FOREIGN KEY (cpf_func) REFERENCES funcionario(cpf_func)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (codigo_brinquedo) REFERENCES brinquedo(codigo_brinquedo)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

CREATE TABLE VENDEDOR (
	cpf_func varchar(11) PRIMARY KEY, 
	guiche smallint CHECK (guiche >= 1 AND guiche <= 10),	
	FOREIGN KEY (cpf_func) REFERENCES funcionario(cpf_func)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

CREATE TABLE CLIENTE (
	cpf_cliente varchar(11) PRIMARY KEY,
	cpf_responsavel varchar(11),
	nome_cliente varchar(50) NOT NULL,
	data_nasc date NOT NULL
);

CREATE TABLE INGRESSOS (
	numIngresso varchar(6) PRIMARY KEY,
	cpf_cliente varchar(11) NOT NULL,
	cpf_func varchar(11) NOT NULL,
	valor DECIMAL(5,2) NOT NULL CHECK (valor >= 0), -- O ingresso pode ser 023,50 ou 124,56 por exemplo.
	data_compra date NOT NULL,
	data_uso date NOT NULL CHECK (data_uso >= data_compra),
	FOREIGN KEY (cpf_func) REFERENCES vendedor(cpf_func)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (cpf_cliente) REFERENCES cliente(cpf_cliente)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);
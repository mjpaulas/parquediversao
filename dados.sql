-- Criação de brinquedos
INSERT INTO brinquedo VALUES ('00000', 'Montanha Russa Radical ao Extremo', '12:30', '20:00', 16, 65, 50);
INSERT INTO brinquedo VALUES ('00001', 'Montanha Russa KiMedo', '10:00', '18:20', 12, 100, 35);
INSERT INTO brinquedo VALUES ('00010', 'Montanha Russa do Mickey Naice', '11:30', '20:00', 8, 100, 50);
INSERT INTO brinquedo VALUES ('00011', 'Trenzinho da Palha Assada', '18:00', '20:00', 0, 100, 25);
INSERT INTO brinquedo VALUES ('00100', 'Rodinha Pouco Gigante', '10:00', '20:00', 3, 10, 6);
INSERT INTO brinquedo VALUES ('00101', 'Gira-Gira Fofinho', '10:00', '18:00', 0, 5, 6);
INSERT INTO brinquedo VALUES ('00111', 'Brinquedo Inflável', '11:00', '17:45', 4, 12, 10);

-- Criação de funcionários
INSERT INTO funcionario VALUES ('43145814842', 'Monitor', 'Paula Caires Silva', '968520023');
INSERT INTO funcionario VALUES ('34233235586', 'Vendedor', 'Giovanni Alvarenga Silva', '981005099');
INSERT INTO funcionario VALUES ('89630197561', 'Monitor', 'Sahudy Montenegro González', '975596109', '973178956');
INSERT INTO funcionario VALUES ('56536706692', 'Vendedor', 'Olavo Brás Martins Bilac', '970474840');
INSERT INTO funcionario VALUES ('34039219309', 'Monitor', 'Fernando Antônio Nogueira Pessoa', '904113623', '951521396');
INSERT INTO funcionario VALUES ('23862527280', 'Vendedor', 'Luís Vaz de Camões', '920673019');
INSERT INTO funcionario VALUES ('89872935807', 'Monitor', 'Chaya Pinkhasivna Lispector', '985034800');
INSERT INTO funcionario VALUES ('52384432427', 'Monitor', 'Eça de Queirós', '919379168');
INSERT INTO funcionario VALUES ('58721973490', 'Monitor', 'Carlos Drummond de Andrade', '924913506');
INSERT INTO funcionario VALUES ('36704974474', 'Monitor', 'Graciliano Ramos de Oliveira', '904308527');
INSERT INTO funcionario VALUES ('08691757507', 'Monitor', 'Euclides Rodrigues Pimenta da Cunha', '975932875');
INSERT INTO funcionario VALUES ('97341779829', 'Monitor', 'João Guimarães Rosa', '905384012');
INSERT INTO funcionario VALUES ('26341633718', 'Monitor', 'Antônio Frederico de Castro Alves', '992679039');
INSERT INTO funcionario VALUES ('78064456924', 'Monitor', 'Mario de Miranda Quintana', '933114993');
INSERT INTO funcionario VALUES ('18315420305', 'Monitor', 'Ana Maria Machado', '928584647', '992679039');
INSERT INTO funcionario VALUES ('66167793867', 'Monitor', 'João Cabral de Melo Neto', '903862826', '924913506');
INSERT INTO funcionario VALUES ('12191099750', 'Monitor', 'Joaquim Maria Machado de Assis', '917092172', '919379168');
INSERT INTO funcionario VALUES ('43124841123', 'Vendedor', 'Manuel Antônio Álvares de Azevedo', '930854731');
INSERT INTO funcionario VALUES ('51611465168', 'Vendedor', 'Luís Nicolau Fagundes Varela', '921268069');

-- Criação de monitores

INSERT INTO monitor VALUES ('97341779829', '00000');
INSERT INTO monitor VALUES ('26341633718', '00000');
INSERT INTO monitor VALUES ('78064456924', '00001');
INSERT INTO monitor VALUES ('18315420305', '00001');
INSERT INTO monitor VALUES ('66167793867', '00010');
INSERT INTO monitor VALUES ('12191099750', '00010');
INSERT INTO monitor VALUES ('43145814842', '00011');
INSERT INTO monitor VALUES ('89630197561', '00011');
INSERT INTO monitor VALUES ('34039219309', '00100');
INSERT INTO monitor VALUES ('89872935807', '00100');
INSERT INTO monitor VALUES ('52384432427', '00101');
INSERT INTO monitor VALUES ('58721973490', '00101');
INSERT INTO monitor VALUES ('36704974474', '00111');
INSERT INTO monitor VALUES ('08691757507', '00111');

-- Criação de vendedores
INSERT INTO vendedor VALUES ('34233235586', '1');
INSERT INTO vendedor VALUES ('56536706692', '2');
INSERT INTO vendedor VALUES ('23862527280');
INSERT INTO vendedor VALUES ('43124841123', '5');
INSERT INTO vendedor VALUES ('51611465168', '7');

-- Criação de Clientes
INSERT INTO cliente VALUES ('14858439100', '', 'Eslovênia Marques de Lima', '27/01/2000');
INSERT INTO cliente VALUES ('96505147756', '', 'Jessilane Alves de Souza', '02/01/1960');
INSERT INTO cliente VALUES ('72460345798', '', 'Lucas Bissoli Garcia', '16/01/1998');
INSERT INTO cliente VALUES ('11541464729', '', 'Natália Rocha Deodato', '15/07/1997');

-- Criação de Clientes dependentes
INSERT INTO cliente VALUES ('50933747500', '11541464729', 'Jade Picon Froes', '27/05/2015');
INSERT INTO cliente VALUES ('55922584618', '11541464729', 'Eliezer do Carmo Neto', '27/05/1940');
INSERT INTO cliente VALUES ('21453009450', '72460345798', 'Paulo André Camilo de Oliveira', '18/02/1950');
INSERT INTO cliente VALUES ('67763561149', '72460345798', 'Lina Pereira dos Santos', '27/05/2014');
INSERT INTO cliente VALUES ('22132498067', '96505147756', 'Larissa Tomásia Arruda', '25/12/1945');
INSERT INTO cliente VALUES ('66443923855', '96505147756', 'Luciano Estevan Neto', '23/12/1930');

-- Criação de ingressos
INSERT INTO ingressos VALUES ('000001', '14858439100', '34233235586', 100.50, '12/05/2020', '13/11/2020');
INSERT INTO ingressos VALUES ('000011', '96505147756', '34233235586', 50.50, '12/04/2020', '13/03/2021');
INSERT INTO ingressos VALUES ('000100', '72460345798', '34233235586', 35.50, '17/12/2015', '13/11/2017');

INSERT INTO ingressos VALUES ('000101', '55922584618', '34233235586', 100.50, '12/12/2020', '17/11/2024');
INSERT INTO ingressos VALUES ('000110', '67763561149', '56536706692', 75.90, '17/09/2014', '15/05/2015');
INSERT INTO ingressos VALUES ('000111', '66443923855', '43124841123', 25.15, '06/12/2015', '06/12/2020');
INSERT INTO ingressos VALUES ('001000', '22132498067', '51611465168', 25.15, '12/11/2020', '13/11/2020');
